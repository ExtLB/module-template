Ext.define('Client.view.<%= options.snakeName %>.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.<%= options.snakeName %>',

	onItemSelected: function (sender, record) {
		Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
	},

	onConfirm: function (choice) {
		if (choice === 'yes') {
			//
		}
	}
});
