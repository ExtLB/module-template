Ext.define('Client.view.<%= options.snakeName %>.View',{
	extend: 'Ext.grid.Grid',
	xtype: '<%= options.snakeName %>',
	cls: '<%= options.snakeName %>view',
	requires: [],
	controller: {type: '<%= options.snakeName %>'},
	viewModel: {type: '<%= options.snakeName %>'},
	store: {type: '<%= options.snakeName %>'},
  perspectives: ['main'],
	selectable: { mode: 'single' },
	listeners: {
		select: 'onItemSelected'
	},
	columns: [
		{
			text: 'Name',
			dataIndex: 'name',
			width: 100,
			cell: {userCls: 'bold'}
		},
		{text: 'Email',dataIndex: 'email',width: 230},
		{
			text: 'Phone',
			dataIndex: 'phone',
			width: 150
		}
	]
});
