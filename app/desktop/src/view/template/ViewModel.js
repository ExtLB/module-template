Ext.define('Client.view.<%= options.snakeName %>.ViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.<%= options.snakeName %>',
	data: {
		name: 'Client'
	}
});
