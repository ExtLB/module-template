let log = require('@dosarrest/loopback-component-logger')('mod-<%= options.name %>/boot');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  init() {
    let me = this;
    let app = me.app;
    let done = me.done;
    let registry = app.registry;
    let MainNavigation = registry.getModelByType('MainNavigation');
    MainNavigation.find({where: {routeId: '<%= options.name %>', perspective: 'main'}}).then(navItem => {
      if (navItem.length === 0) {
        MainNavigation.create({
          text: 'mod-<%= options.name %>:<%= options.name.toUpperCase() %>',
          perspective: 'main',
          iconCls: 'x-fa fa-users',
          // rowCls: 'nav-tree-badge nav-tree-badge-new',
          viewType: '<%= options.snakeName %>',
          routeId: '<%= options.name %>',
          leaf: true,
        }).then(newNavItem => {
          log.info(newNavItem);
          done(null, true);
        }).catch(err => {
          log.error(err);
          done(null, false);
        });
      } else {
        done(null, true);
      }
    }).catch(err => {
      log.error(err);
      done(null, true);
    });
  }
}
module.exports = Boot;
